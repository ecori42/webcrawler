package com.ecori.webCrawler;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Wrapper class for crawled results. Stores detected url and pattern snippet.
 * 
 * @author EcoRI
 */
public class CrawlResult implements Serializable {

	// UID
	private static final long serialVersionUID = 6223172442606487926L;

	// detected pattern snippet
	private String resultPattern;
	
	// detected URL
	private String resultURL;

	/**
	 * Getter and Setter
	 */
	
	public String getResultPattern() {
		return resultPattern;
	}

	public void setResultPattern(String resultPattern) {
		this.resultPattern = resultPattern;
	}

	public String getResultURL() {
		return resultURL;
	}

	public void setResultURL(String resultURL) {
		this.resultURL = resultURL;
	}




}
