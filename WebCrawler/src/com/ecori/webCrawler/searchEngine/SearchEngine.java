package com.ecori.webCrawler.searchEngine;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * Search Engine holds search algorithm and search functionality.
 * 
 * @author EcoRI
 */
public class SearchEngine {

	/**
	 * Performs a search on the body of on the HTML document that is retrieved.
	 * This method should only be called after a successful crawl.
	 * 
	 * @param searchWord
	 *            - The word or string to look for
	 * @return whether or not the word was found
	 */

	/**
	 * Applys knuth-morris-pratt pattern detection algorithmn on body content of
	 * given Document Objects (storing a crawled html document). Returns first
	 * detected pattern snippet.
	 * 
	 * @param pSearchWord String representingthe pattern to search
	 * @param pText Document objects holding the body content 
	 * @return
	 */
	public String applySearch(String pSearchWord, Document pText) {

		// apply search algo on body part of html page
		int pos = KnuthMorrisPrattSearch(pSearchWord, pText.body().text());
		
		// prepare output snippet
		if (pos >= 0) {

			if (pos == 0) {
				return pText.body().text().substring(pos, pos + pSearchWord.length() + 10);
			} else if (pos == 1) {
				return pText.body().text().substring(pos - 1, pos + pSearchWord.length() + 10);
			} else if(pos > 10) {
				return pText.body().text().substring(pos - 10, pos + pSearchWord.length() + 10);
			} else{
				return pText.body().text().substring(pos - 5, pos + pSearchWord.length() + 5);				
			}
			// nothing detected
		} else {
			return "";
		}
	}

	/**
	 * KnuthMorrisPrattSearch
	 * 
	 * Pattern detection algo in O(m+n).
	 * From http://www.baeldung.com/java-full-text-search-algorithms
	 * 
	 * @param pPattern pattern
	 * @param pText Text to search
	 * @return position of first hit
	 */
	public static int KnuthMorrisPrattSearch(String pPattern, String pText) {

		// transform to array
		char[] text = pText.toCharArray();
		char[] pattern = pPattern.toCharArray();
		int patternSize = pattern.length;
		int textSize = text.length;

		int i = 0, j = 0;

		// create mask
		int[] shift = KnuthMorrisPrattShift(pattern);

		// apply pattern detection
		while ((i + patternSize) <= textSize) {
			while (text[i + j] == pattern[j]) {
				j += 1;
				if (j >= patternSize)
					return i;
			}

			if (j > 0) {
				i += shift[j - 1];
				j = Math.max(j - shift[j - 1], 0);
			} else {
				i++;
				j = 0;
			}
		}
		return -1;
	}

	/**
	 * KnuthMorrisPrattShift
	 * 
	 * Shift table for KnuthMorrisPratt algo.
	 * 
	 * From http://www.baeldung.com/java-full-text-search-algorithms
	 * 
	 * @param pattern
	 * @return
	 */
	public static int[] KnuthMorrisPrattShift(char[] pattern) {
		
		int patternSize = pattern.length;
		int[] shift = new int[patternSize];
		shift[0] = 1;

		int i = 1, j = 0;

		// create mask for pattern detection
		while ((i + j) < patternSize) {
			if (pattern[i + j] == pattern[j]) {
				shift[i + j] = i;
				j++;
			} else {
				if (j == 0)
					shift[i] = i + 1;

				if (j > 0) {
					i = i + shift[j - 1];
					j = Math.max(j - shift[j - 1], 0);
				} else {
					i = i + 1;
					j = 0;
				}
			}
		}
		return shift;
	}
}