package com.ecori.webCrawler;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;

import org.jsoup.nodes.Document;

/**
 * WebBean bean to store result list and user input/output.
 * 
 * @author EcoRI
 */
@ManagedBean
@ViewScoped
public class WebBean implements Serializable{
	
	// UID
	private static final long serialVersionUID = -1270197098476152477L;

	// user entered root url to crawl
	private String url = "";
	
	// pattern to search crawled pages
	private String pattern = "";
	
	// user output
	private StringBuffer output;

	// holds Crawled results: url to page and sniped of detected pattern
	private List<CrawlResult> resultList = new ArrayList<CrawlResult>();

	
	/**
	 * Getter and Setter
	 */
	
	public List<CrawlResult> getResultList() {
		return resultList;
	}

	public void setResultList(List<CrawlResult> resultList) {
		this.resultList = resultList;
	}

	
	public StringBuffer getOutput() {
		
		if(output == null){
			output = new StringBuffer().append("");
		}
		return output;
	}

	public void setOutput(StringBuffer output) {
		this.output = output;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}  
}