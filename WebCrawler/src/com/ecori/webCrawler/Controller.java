package com.ecori.webCrawler;

import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.ecori.webCrawler.searchEngine.SearchEngine;

/**
 * Main controller for web crawling application. Holds in-memory index
 * represented by some data structures and crawling functionality. No further
 * encapsulation and action facade(s) are used due to simplicity.
 * 
 * @author EcoRI
 *
 */
@ManagedBean
@ViewScoped
public class Controller implements Serializable {

	// UID
	private static final long serialVersionUID = -4731759340348571404L;

	// Using fake USER_AGENT to prevent robot from detection.
	private static final String USER_AGENT = "Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6";

	// webbean holds user entered data
	@ManagedProperty(value = "#{webBean}")
	private WebBean webBean;

	// limit for pages to crawl
	private static final int MAX_PAGMAX_PAGES_TO_CRAWL = 100;

	/**
	 * Index part. In-memory storage.
	 */
	private Set<String> crawledPages = new LinkedHashSet<String>();
	private List<String> pagesToCrawl = new LinkedList<String>();
	private Set<String> encounteredLinks = new LinkedHashSet<String>();

	// store crawled pages
	private Map<String, Document> crawlMap = new ConcurrentHashMap<String, Document>();

	// Search Engine offering pattern detection
	private SearchEngine searchEngine = new SearchEngine();

	/**
	 * User click event activated search on crawled results in-memory.
	 */
	public void onSearchClicked() {

		this.webBean.getOutput().append("Begin search...");

		// clear displayed results in ui for better viewing purpose
		webBean.getResultList().clear();

		String match;

		// iterate over extracted crawled results and apply pattern detection on
		// text content
		for (Entry<String, Document> entry : crawlMap.entrySet()) {

			match = searchEngine.applySearch(webBean.getPattern(), entry.getValue());

			// sort out no matches
			if (match == null || match.trim().isEmpty()) {
				continue;
			}

			// store results in-memory
			CrawlResult res = new CrawlResult();
			res.setResultPattern(match);
			res.setResultURL(entry.getKey());

			if(!webBean.getResultList().contains(res)){
				webBean.getResultList().add(res);
			}
		}
	}

	/**
	 * User click event activated crawling.
	 */
	public void onCrawlClicked() {

		// clear buffer
		this.webBean.getOutput().delete(0, this.webBean.getOutput().length());

		// user info
		this.webBean.getOutput().append("Crawling... " + this.webBean.getUrl());

		// start crawling
		try {
			getURLCrawl(this.webBean.getUrl());
			// crawlDocument(webBean.getUrl());
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Crawl given URL and all encountered links (if they are on hostname) and
	 * store in-memory.
	 * 
	 * @param url
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	private void crawlURL(String pURL) throws IOException, URISyntaxException {

		// connect to given URL as user agent, referrer is set to google
		Response response = Jsoup.connect(pURL).userAgent(USER_AGENT).ignoreContentType(true)
		           .referrer("http://www.google.com")   
		           .timeout(12000) 
		           .followRedirects(true)
		           .execute();
	
		// check for status code OK
		if (response.statusCode() != 200) {
			return;
		}

		// text to consider
		if (!response.contentType().contains("text/html")) {
			return;
		}
		// parse html content
		Document htmlDocument = response.parse();

		// extract links from document
		getLinksFromDocument(htmlDocument, pURL);

		// store in-memory in map
		crawlMap.put(pURL, htmlDocument);

	}

	/**
	 * Extracts links on given Document holding a html page to im-memory
	 * storage.
	 * 
	 * @param pDoc
	 *            Document objects holding the crawled page.
	 * @param pRootUrl
	 *            String RootURL where links are encountered.
	 * 
	 * @throws URISyntaxException
	 */
	private void getLinksFromDocument(Document pDoc, String pRootUrl) throws URISyntaxException {

		Set<String> ret = new HashSet<String>();

		// get all links
		Elements linksOnPage = pDoc.select("a[href]");

		// for all encountered links
		for (Element link : linksOnPage) {

			// extract hostname from given root URL
			String hostname = getHostName(pRootUrl);

			// consider only links on same hostname from given root URL
			if (link.absUrl("href").contains(hostname)) {

				// store in-memory
				if (!ret.contains(link)) {
					this.encounteredLinks.add(link.absUrl("href"));
				}
			}

		}

	}

	/**
	 * Get hostname from fiven URL.
	 * 
	 * @param pURL
	 * @return
	 * @throws URISyntaxException
	 */
	private String getHostName(String pURL) throws URISyntaxException {

		URI uri = null;

		try {
			// transform to URI object
			uri = new URI(pURL);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String hostname = uri.getHost();

		// remove www and check for npe
		if (hostname != null) {
			return hostname.startsWith("www.") ? hostname.substring(4) : hostname;
		}
		return hostname;
	}

	/**
	 * Main crawling method. Crawl given URL and encountered links and stores
	 * crawled results in-memory.
	 * 
	 * @param pURL
	 *            String URL to crawl
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	private void getURLCrawl(String pURL) throws IOException, URISyntaxException {

		// reset in-memory
		encounteredLinks.clear();
		crawledPages.clear();
		pagesToCrawl.clear();

		// crawl ini
		while (this.crawledPages.size() < MAX_PAGMAX_PAGES_TO_CRAWL) {

			String currentUrl;

			if (this.pagesToCrawl.isEmpty()) {
				currentUrl = pURL;
				this.crawledPages.add(pURL);
			} else {
				currentUrl = this.getNextURL();
			}

			// crawl url and links
			crawlURL(currentUrl);

			// add links to crawl list
			this.pagesToCrawl.addAll(encounteredLinks);
		}
	}

	/**
	 * Helper method to return next url from list.
	 * 
	 * @return String
	 */
	private String getNextURL() {

		String nextUrl;
		do {

			nextUrl = this.pagesToCrawl.remove(0);

		} while (this.crawledPages.contains(nextUrl));

		this.crawledPages.add(nextUrl);
		return nextUrl;
	}

	/**
	 * Getter and Setter
	 */

	public WebBean getWebBean() {
		return webBean;
	}

	public void setWebBean(WebBean webBean) {
		this.webBean = webBean;
	}
}