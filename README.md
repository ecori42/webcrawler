## Webcrawler Application

Here a simple Webcrawler + Pattern Search Webapplication is presented. This project was part of a code challange with an fixed amount of time - around 4h. 
The application allows users to crawl a given URL and all links on the same hostname and provides pattern detection on the crawled pages. 
The text-search algorithm used here is Knuth-Morri-Pratt-Algorithm which has a timecomplexity of O(m+n) with will detect the first occurances of an given pattern. 
Results are stored in-memory. Crawling is separated from the seaching process, so users can crawl and simultaneously seach the growing list of crawled pages.
---

## Simple Workflow

Here a simple workflow is provided:

1. Enter setup java application and deploy on local server.
2. Visit: http://localhost:8080/WebCrawler/crawler.xhtml or http://localhost:8082/WebCrawler/crawler.xhtml
3. Enter URL for root url to crawl
4. Enter pattern for text detection. you need to crawl at least one page, so your search can find something ;). You can search different pattern in crawled pages. Search result will grow with crawlingprocess.
5. Results are displayed in crapy html. Each seach will reset result view due to visibility.
